package com.experiments.inotifypoc;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.TextView;

import com.experiments.inotifypoc.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    // NOTE: The same vulnerability can be triggered by native code (through the inotify API) in a more stealty way!
    // TODO: add a PoC of inotify from native code

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        if (!Settings.canDrawOverlays(this)) {
            StringBuilder sb = new StringBuilder();
            sb.append("package:");
            sb.append(getPackageName());
            Intent intent = new
                    Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION", Uri.parse(sb.toString()));

            ActivityResultLauncher<Intent> activityLauncher =
                    registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    // TODO: Check and react if user does not allow the permission
                    Log.d(TAG, "Result code : " + result.getResultCode());
                }
            });

            activityLauncher.launch(intent);
        }

        // Start service
        // TODO: make service persistant in background
        startService(new Intent(getApplicationContext(), MonitoringService.class));

        // Example of a call to a native method
        TextView tv = binding.sampleText;
        tv.setText("Inotify POC Main Activity");
    }
}