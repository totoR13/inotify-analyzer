package com.experiments.inotifypoc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class FakeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fake);

        // Retrieve info from bundle extra
        String targetPackage = getIntent().getExtras().getString("targetPackage", "NONE");

        TextView tv = findViewById(R.id.textViewReplaced);
        tv.setText("Start app \"" + targetPackage + "\" detected");
    }
}