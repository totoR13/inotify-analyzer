# InotifyPOC

InotifyPOC is an Android application to exploit inotify.

## Prerequisites

Before building and executing the InotifyPoc app, please check:
* `AndroidManifest.xml` - depending on the device API and/or the target API level, specific permissions may be required;
* `com.experiments.inotifypoc.MonitoringService.java` - update the `TARGET_PACKAGES` variable with the package name of the target applications.
