# ❱ Inotify on Android

## ❱ Repository structure

This repository contains the eBPF scripts to analyze the file system events in an Android device and a POC app 

The repo contains the following folders:

* `fsanalyzer` - the eBPF script used to collect the file system events in the device;
* `InotifyPOC` - a POC Android application to perform a phishing attack exploiting inotify on the installation path.
<!--* `media` - a folder with images and videos.-->

For more details, please refer to the `READMEs` in the specific folders.

## ❱ Pubblication

We submit it for consideration to [Euro S&P 2023](https://eurosp2023.ieee-security.org/).

## ❱ Licencing
This tool is available under the [GPL license](license).

## ❱ Credits
[![Unige](https://intranet.dibris.unige.it/img/logo_unige.gif)](https://unige.it/en/)
[![Dibris](https://intranet.dibris.unige.it/img/logo_dibris.gif)](https://www.dibris.unige.it/en/)

[![EURECOM](https://www.eurecom.fr/themes/custom/eurecom/images/EURECOM_logo_250x118.png)](https://www.eurecom.fr/en)

## ❱ Team 

* Antonio Ruggia - PhD. Student
* Andrea Possemato
* Alessio Merlo - Associate Professor
* Dario Nisi - Postdoctoral Researcher
* Simone Aonzo - Associate Professor
