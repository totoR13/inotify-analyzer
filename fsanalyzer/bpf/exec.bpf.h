#include <linux/types.h>
#ifdef asm_inline
#undef asm_inline
#define asm_inline asm
#endif

#include <uapi/linux/ptrace.h>
#include <uapi/linux/bpf.h>
#include <linux/fs.h>

#include <bpf_helpers.h>
#include <bpf_tracing.h>
#include <bpf_core_read.h>     /* for BPF CO-RE helpers -- https://facebookmicrosites.github.io/bpf/blog/2020/02/20/bcc-to-libbpf-howto-guide.html */

// ================== Constants ==================

#define MAX_KPROBE_NAME 12
#define MAX_PARAMS 4
#define MAX_BUFFER 128*4

#define MAX_OPENED_FILES 50

// ================== Memory operations ==================

// NOTE: Loop only from linux kernel >= 5.2 
static __always_inline bool my_strcmp(char *str1, char *str2, int n) {
    // #pragma unroll
    for (int i = 0; i < n; i++) {
        if (str1[i] != str2[i]) {
            return false;
        }
        if (str1[i] == '\0') {
            return true;
        }
    }
    return false;
}

static __always_inline int my_strcpy(char *source, char *dest, int max_length) {
    int len = 0;
    for (int i = 0; i < max_length; i++) {
        if (source[i] == '\0') {
            dest[i] = '\0';
            break;
        } else {
            dest[i] = source[i];
            len++;
        }
    }

    // return len-1; // Do not count the final \0
    return len;
}

static __always_inline void my_path_append(char *source, char *dest, int max_length, int start_index) {
    if (start_index >= max_length) {
        return;
    }
    dest[start_index] = '/';
    for (int i = 0; i < max_length; i++) {
        int inner_index = i+start_index+1;
        if (inner_index >= max_length) {
            dest[max_length-1] = '\0';
            break;
        }
        if (source[i] == '\0') {
            dest[inner_index] = '\0';
            break;
        } else {
            dest[inner_index] = source[i];
        }
    }
} 


// ================== Define General eBPF Structures ==================

#define BPF_MAP(_name, _type, _key_type, _value_type, _max_entries) \
    struct bpf_map_def SEC("maps") _name = {                        \
        .type = _type,                                              \
        .key_size = sizeof(_key_type),                              \
        .value_size = sizeof(_value_type),                          \
        .max_entries = _max_entries,                                \
    };

#define BPF_HASH(_name, _key_type, _value_type) \
    BPF_MAP(_name, BPF_MAP_TYPE_HASH, _key_type, _value_type, 2048);

#define BPF_PERF_OUTPUT(_name) \
    BPF_MAP(_name, BPF_MAP_TYPE_PERF_EVENT_ARRAY, int, __u32, (4096 * 1/*2084*/ /*sizeof(struct event_result_t)*/)); // 4132

#define BPF_ARRAY(_name, _value_type, _max_entries) \
    BPF_MAP(_name, BPF_MAP_TYPE_ARRAY, u32, _value_type, _max_entries);

// https://www.ferrisellis.com/content/ebpf_syscall_and_maps/#bpf_map_type_percpu_hash-and-bpf_map_type_percpu_array
#define BPF_PERCPU_HASH(_name, _key_type, _value_type) \
    BPF_MAP(_name, BPF_MAP_TYPE_PERCPU_HASH, _key_type, _value_type, 2048);

#define READ_KERN(ptr) ({ typeof(ptr) _val;                             \
                          __builtin_memset(&_val, 0, sizeof(_val));     \
                          bpf_probe_read(&_val, sizeof(_val), &ptr);    \
                          _val;                                         \
                        })

#if defined(bpf_target_x86)
#define PT_REGS_PARM6(ctx)  ((ctx)->r9)
#elif defined(bpf_target_arm64)
#define PT_REGS_PARM6(x) (((PT_REGS_ARM64 *)(x))->regs[5])
#endif

static __always_inline bool is_x86_compat(struct task_struct *task) {
#if defined(bpf_target_x86)
    return READ_KERN(task->thread_info.status) & TS_COMPAT;
#else
    return false;
#endif
}

struct syscall_args_t {
    u32 syscall;
    unsigned long args[6];
};
BPF_HASH(syscall_args_map, u64, struct syscall_args_t);

// ================== Helper functions ==================

static __always_inline int read_sys_enter_args(struct syscall_args_t *args, void *sys_enter_input) {
    args->syscall = -1;
    struct task_struct *task = (struct task_struct *)bpf_get_current_task();

    struct bpf_raw_tracepoint_args *ctx = (struct bpf_raw_tracepoint_args *) sys_enter_input;
    
    args->syscall = ctx->args[1];
#if defined(CONFIG_ARCH_HAS_SYSCALL_WRAPPER)
    struct pt_regs regs = {};
    bpf_probe_read(&regs, sizeof(struct pt_regs), (void*)ctx->args[0]);

    if (is_x86_compat(task)) {
#if defined(bpf_target_x86)
        args->args[0] = regs.bx;
        args->args[1] = regs.cx;
        args->args[2] = regs.dx;
        args->args[3] = regs.si;
        args->args[4] = regs.di;
        args->args[5] = regs.bp;
#endif // bpf_target_x86
    } else {
        args->args[0] = PT_REGS_PARM1(&regs);
        args->args[1] = PT_REGS_PARM2(&regs);
        args->args[2] = PT_REGS_PARM3(&regs);
        // args->args[3] = PT_REGS_PARM4(&regs);
        args->args[4] = PT_REGS_PARM5(&regs);
        args->args[5] = PT_REGS_PARM6(&regs);

        // fix for syscalls
        args->args[3] = PT_REGS_PARM4_SYSCALL(&regs);
    }
#else 
    args->args[0] = ctx->args[0];
    args->args[1] = ctx->args[1];
    args->args[2] = ctx->args[2];
    args->args[3] = ctx->args[3];
    args->args[4] = ctx->args[4];
    args->args[5] = ctx->args[5];
#endif 

    return 0;
}

static __always_inline u64 array_to_int(u8 * buffer, u8 size) {
    // e.g., (tmp[0]) | (tmp[1] << 8) | (tmp[2] << 16) | (tmp[3] << 24)
    u64 result = 0;
    for (int i = 0; i < size; i++) {
        result |= (buffer[i] << (8 * i));
    }
    return result;
}

static __always_inline u64 get_current_pid() {
    u32 pid = bpf_get_current_pid_tgid();
    return (u64) pid;
}

static __always_inline u64 get_current_tgid() {
    u64 res = bpf_get_current_pid_tgid();
    return (u64) res >> 32;
}

static __always_inline u64 get_current_uid() {
    u32 uid = bpf_get_current_uid_gid();
    return (u64) uid;
}

static __always_inline u64 get_syscall_map_key(u32 syscall_id) {
    u64 id = syscall_id;
    u32 pid = get_current_pid();
    id = id << 32 | pid;

    return id;
}

static __always_inline u64 get_fd2path_map_key(int fd) {
    u32 pid = get_current_pid();
    u64 id = ((u64)fd) << 32 | pid;

    return id;
}
