#include "exec.bpf.h"

#include <uapi/linux/bpf.h>
#include <linux/dcache.h>
#include <linux/err.h>
#include <linux/fdtable.h>
#include <linux/fs.h>
#include <linux/fs_struct.h>
#include <linux/path.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/unistd.h> // For the __NR_* variables

struct fd2path_t {
    bool is_active;
    uint64_t pid;
    int fd;
    int index;
    char original_fname[MAX_BUFFER];
};
BPF_HASH(fd2path_map, u64, struct fd2path_t);

struct open_mod_t {
    uint64_t mode; // mode or flags
    char args[MAX_PARAMS][MAX_BUFFER];
};

struct event_result_t {
    uint64_t timestamp;
    uint64_t pid;
    uint64_t uid;
    char syscall[MAX_KPROBE_NAME];
    struct open_mod_t result;
};
BPF_PERF_OUTPUT(event_result_perf_map);

struct cache_array_t {
    struct fd2path_t values[MAX_OPENED_FILES];
};
static struct cache_array_t cached_array;

static int get_first_free_index() {
    for (int i = 0; i < MAX_OPENED_FILES; i++) {
        if (cached_array.values[i].is_active) {
            continue;
        }

        return i;
    }

    return -1;
}

static bool add_new_fd2path(int fd, char *folder, char *filename) {

    if (fd <= 0 || filename == NULL || filename[0] == '\0') {
        return false;
    }

    u64 id = get_fd2path_map_key(fd);
    uint64_t pid = get_current_pid();
    int index = get_first_free_index();

    if (index >= 0 && index < MAX_OPENED_FILES) {
        cached_array.values[index].is_active = true;
        cached_array.values[index].fd = fd;
        cached_array.values[index].pid = pid;
        cached_array.values[index].index = index;
        my_strcpy(filename, cached_array.values[index].original_fname, sizeof(cached_array.values[index].original_fname));
  
        bpf_map_update_elem(&fd2path_map, &id, &cached_array.values[index], BPF_ANY);

        return true;
    }

    return false;
}

static void remove_fd2path(int fd) {
    if (fd <= 0) {
        return;
    }

    u64 id = get_fd2path_map_key(fd);
    struct fd2path_t *fd2path = bpf_map_lookup_elem(&fd2path_map, &id);
    if (fd2path == NULL) {
        return;
    }

    int index = fd2path->index;
    if (index < 0 || index >= MAX_OPENED_FILES) {
        return;
    }

    cached_array.values[index].is_active = false;
    bpf_map_delete_elem(&fd2path_map, &id);
}

static bool clone_fd2path(int new_fd, int original_fd) {
    u64 id = get_fd2path_map_key(original_fd);
    struct fd2path_t *fd2path = bpf_map_lookup_elem(&fd2path_map, &id);
    if (fd2path == NULL) {
        return false;
    }

    return add_new_fd2path(new_fd, NULL, fd2path->original_fname);
}

bool get_path_from_fd(uint32_t fd, char *filename, uint32_t size) {
    if (fd < 0) {
        return false;
    }

    u64 id = get_fd2path_map_key(fd);
    struct fd2path_t *fd2path = bpf_map_lookup_elem(&fd2path_map, &id);
    if (fd2path == NULL) {
        return false;
    }

    my_strcpy(fd2path->original_fname, filename, size);
    return true;
}

void log_event(void *ctx, struct event_result_t *res) {
    if (res == NULL) {
        return;
    }

    res->timestamp = bpf_ktime_get_boot_ns();
    res->pid = get_current_pid();
    res->uid = get_current_uid();
    bpf_perf_event_output(ctx, &event_result_perf_map, BPF_F_CURRENT_CPU, res, sizeof(struct event_result_t));
}

// Generic functions
void handle_fd(struct syscall_args_t *saved_args, struct event_result_t *res) {
    // Read the mode
    res->result.mode = 0;

    // Read the fd 
    get_path_from_fd(saved_args->args[0], res->result.args[0], sizeof(res->result.args[0]));
    
    res->result.args[1][0] = '\0';
    res->result.args[2][0] = '\0';
    res->result.args[3][0] = '\0';
}

void handle_fd_and_filename(struct syscall_args_t *saved_args, struct event_result_t *res) {
    // Read the mode
    res->result.mode = 0;

    // Read the filename and (eventually) fd
    bpf_probe_read_str(&res->result.args[1], sizeof(res->result.args[1]), (void *) saved_args->args[1]);
    if (res->result.args[1][0] == '/') {
        res->result.args[0][0] = '\0';
    } else {
        // Read the fd 
        get_path_from_fd(saved_args->args[0], res->result.args[0], sizeof(res->result.args[0]));
    }

    res->result.args[2][0] = '\0';
    res->result.args[3][0] = '\0';
}

void handle_filename(struct syscall_args_t *saved_args, struct event_result_t *res) {
    // Read the mode
    res->result.mode = 0;

    // Read the filename
    bpf_probe_read_str(&res->result.args[0], sizeof(res->result.args[0]), (void *) saved_args->args[0]);

    res->result.args[1][0] = '\0';
    res->result.args[2][0] = '\0';
    res->result.args[3][0] = '\0';
}

void handle_new_old_filename(struct syscall_args_t *saved_args, struct event_result_t *res) {
    // Read the mode
    res->result.mode = 0;

    // Read the filename
    bpf_probe_read_str(&res->result.args[0], sizeof(res->result.args[0]), (void *) saved_args->args[0]);
    bpf_probe_read_str(&res->result.args[1], sizeof(res->result.args[1]), (void *) saved_args->args[1]);

    res->result.args[2][0] = '\0';
    res->result.args[3][0] = '\0';
}

void handle_new_old_fd_and_filename(struct syscall_args_t *saved_args, struct event_result_t *res) {
    // Read the mode
    res->result.mode = 0;

    // Read the old filename and (eventually) fd
    bpf_probe_read_str(&res->result.args[1], sizeof(res->result.args[1]), (void *) saved_args->args[1]);
    if (res->result.args[1][0] == '/') {
        res->result.args[0][0] = '\0';
    } else {
        // Read the fd 
        get_path_from_fd(saved_args->args[0], res->result.args[0], sizeof(res->result.args[0]));
    }

    // Read the new filename and (eventually) fd
    bpf_probe_read_str(&res->result.args[3], sizeof(res->result.args[3]), (void *) saved_args->args[3]);
    if (res->result.args[3][0] == '/') {
        res->result.args[2][0] = '\0';
    } else {
        // Read the fd 
        get_path_from_fd(saved_args->args[2], res->result.args[2], sizeof(res->result.args[2]));
    }
}

// https://stackoverflow.com/questions/49736442/linux-tc-ebpf-and-concurency
// https://lwn.net/Articles/812503/
BPF_PERCPU_HASH(result_map, u64, struct event_result_t);
struct event_result_t *get_syscall_percpu_result(void *ctx, int syscall_id){
    u64 key = (u64) syscall_id;
    return (struct event_result_t *) bpf_map_lookup_elem(&result_map, &key);
}

// ================== Kprobes ==================
// TODO: Big refactor to automate the k(ret)probe creation 

SEC("raw_tracepoint/sys_enter")
int fun__sys_enter(void *ctx) { // struct trace_event_raw_sys_enter *args for kernel < 4.17
    
    u64 uid = get_current_uid();
    if (uid == 2000 /*shell*/) {
        return 0;
    }

    // parse input parameters
    struct syscall_args_t args = {};
    read_sys_enter_args(&args, ctx);

    if (args.syscall < 0 ||
            !(args.syscall == __NR_open ||
                args.syscall == __NR_openat ||
                // args.syscall == SYS_ACCESS ||
                // args.syscall == SYS_FACCESSAT ||
                args.syscall == __NR_dup ||
                args.syscall == __NR_dup2 ||
                args.syscall == __NR_dup3 || 
                args.syscall == __NR_chmod ||
                args.syscall == __NR_chown ||
                args.syscall == __NR_close ||
                args.syscall == __NR_execve ||
                args.syscall == __NR_execveat ||
                args.syscall == __NR_faccessat ||
                args.syscall == __NR_fchmod ||
                args.syscall == __NR_fchmodat ||
                args.syscall == __NR_fchown ||
                args.syscall == __NR_fchownat ||
                args.syscall == __NR_fsetxattr ||
                args.syscall == __NR_ftruncate ||
                args.syscall == __NR_futimesat ||
                args.syscall == __NR_lchown ||
                args.syscall == __NR_link ||
                args.syscall == __NR_linkat ||
                args.syscall == __NR_lsetxattr ||
                args.syscall == __NR_mkdir ||
                args.syscall == __NR_mkdirat ||
                args.syscall == __NR_mknod ||
                args.syscall == __NR_mknodat ||
                args.syscall == __NR_read ||
                args.syscall == __NR_rename ||
                args.syscall == __NR_renameat ||
                args.syscall == __NR_renameat2 ||
                args.syscall == __NR_rmdir ||
                args.syscall == __NR_setxattr ||
                args.syscall == __NR_symlink ||
                args.syscall == __NR_truncate ||
                args.syscall == __NR_unlink ||
                args.syscall == __NR_unlinkat ||
                args.syscall == __NR_utime ||
                args.syscall == __NR_utimensat ||
                args.syscall == __NR_utimes ||
                args.syscall == __NR_write)) {
        return 0;
    }

    // Save parsed args to map
    u64 id = get_syscall_map_key(args.syscall);
    bpf_map_update_elem(&syscall_args_map, &id, &args, BPF_ANY);
 
    return 0;
}

SEC("kretprobe/__x64_sys_close")
int BPF_KRETPROBE(close) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_close);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = get_syscall_percpu_result(ctx, /*__NR_close*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("close", res->syscall, MAX_KPROBE_NAME);
    res->result.mode = 0;

    // Read the filename in the remaining buffer
    bool found = get_path_from_fd(saved_args->args[0], res->result.args[0], sizeof(res->result.args[0]));
    remove_fd2path(saved_args->args[0]);

    res->result.args[1][0] = '\0';
    res->result.args[2][0] = '\0';
    res->result.args[3][0] = '\0';

    if (found) {
        log_event(ctx, res);
    }

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_read")
int BPF_KRETPROBE(read) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_read);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = get_syscall_percpu_result(ctx, /*__NR_read*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("read", res->syscall, MAX_KPROBE_NAME);

    handle_fd(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_execve")
int BPF_KRETPROBE(execve) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_execve);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = get_syscall_percpu_result(ctx, /*__NR_execve*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("execve", res->syscall, MAX_KPROBE_NAME);

    handle_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_execveat")
int BPF_KRETPROBE(execveat) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_execveat);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = get_syscall_percpu_result(ctx, /*__NR_execveat*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("execveat", res->syscall, MAX_KPROBE_NAME);

    handle_fd_and_filename(saved_args, res);
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_chmod")
int BPF_KRETPROBE(chmod) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_chmod);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = get_syscall_percpu_result(ctx, /*__NR_chmod*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("chmod", res->syscall, MAX_KPROBE_NAME);

    handle_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_fchmod")
int BPF_KRETPROBE(fchmod) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_fchmod);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_fchmod*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("fchmod", res->syscall, MAX_KPROBE_NAME);

    handle_fd(saved_args, res);
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_fchmodat")
int BPF_KRETPROBE(fchmodat) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_fchmodat);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_fchmodat*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("fchmodat", res->syscall, MAX_KPROBE_NAME);

    handle_fd_and_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_utimensat")
int BPF_KRETPROBE(utimensat) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_utimensat);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_utimensat*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("utimensat", res->syscall, MAX_KPROBE_NAME);

    handle_fd_and_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_utime")
int BPF_KRETPROBE(utime) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_utime);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_utime*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("utime", res->syscall, MAX_KPROBE_NAME);

    handle_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_utimes")
int BPF_KRETPROBE(utimes) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_utimes);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_utimes*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("utimes", res->syscall, MAX_KPROBE_NAME);

    handle_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_futimesat")
int BPF_KRETPROBE(futimesat) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_futimesat);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_futimesat*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("futimesat", res->syscall, MAX_KPROBE_NAME);

    handle_fd_and_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_setxattr")
int BPF_KRETPROBE(setxattr) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_setxattr);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_setxattr*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("setxattr", res->syscall, MAX_KPROBE_NAME);

    handle_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_lsetxattr")
int BPF_KRETPROBE(lsetxattr) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_lsetxattr);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_lsetxattr*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("lsetxattr", res->syscall, MAX_KPROBE_NAME);

    handle_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_fsetxattr")
int BPF_KRETPROBE(fsetxattr) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_fsetxattr);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_fsetxattr*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("fsetxattr", res->syscall, MAX_KPROBE_NAME);

    handle_fd(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_link")
int BPF_KRETPROBE(link) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_link);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_link*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("link", res->syscall, MAX_KPROBE_NAME);

    handle_new_old_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_linkat")
int BPF_KRETPROBE(linkat) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_linkat);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_linkat*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("linkat", res->syscall, MAX_KPROBE_NAME);

    handle_new_old_fd_and_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_unlink")
int BPF_KRETPROBE(unlink) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_unlink);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_unlink*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("unlink", res->syscall, MAX_KPROBE_NAME);

    handle_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_unlinkat")
int BPF_KRETPROBE(unlinkat) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_unlinkat);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_unlinkat*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("unlinkat", res->syscall, MAX_KPROBE_NAME);

    handle_fd_and_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_chown")
int BPF_KRETPROBE(chown) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_chown);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_chown*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("chown", res->syscall, MAX_KPROBE_NAME);

    handle_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_fchown")
int BPF_KRETPROBE(fchown) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_fchown);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_fchown*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("fchown", res->syscall, MAX_KPROBE_NAME);

    handle_fd(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_lchown")
int BPF_KRETPROBE(lchown) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_lchown);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_lchown*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("lchown", res->syscall, MAX_KPROBE_NAME);

    handle_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_fchownat")
int BPF_KRETPROBE(fchownat) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_fchownat);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_fchownat*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("fchownat", res->syscall, MAX_KPROBE_NAME);

    handle_fd_and_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_mkdir")
int BPF_KRETPROBE(mkdir) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_mkdir);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_mkdir*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("mkdir", res->syscall, MAX_KPROBE_NAME);

    handle_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_mkdirat")
int BPF_KRETPROBE(mkdirat) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_mkdirat);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_mkdirat*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("mkdirat", res->syscall, MAX_KPROBE_NAME);

    handle_fd_and_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_symlink")
int BPF_KRETPROBE(symlink) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_symlink);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_symlink*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("symlink", res->syscall, MAX_KPROBE_NAME);

    handle_new_old_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_mknod")
int BPF_KRETPROBE(mknod) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_mknod);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_mknod*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("mknod", res->syscall, MAX_KPROBE_NAME);

    handle_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_mknodat")
int BPF_KRETPROBE(mknodat) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_mknodat);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_mknodat*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("mknodat", res->syscall, MAX_KPROBE_NAME);

    handle_fd_and_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_rmdir")
int BPF_KRETPROBE(rmdir) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_rmdir);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_rmdir*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("rmdir", res->syscall, MAX_KPROBE_NAME);

    handle_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_write")
int BPF_KRETPROBE(write) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_write);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_write*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("write", res->syscall, MAX_KPROBE_NAME);

    handle_fd(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_truncate")
int BPF_KRETPROBE(truncate) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_truncate);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_truncate*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("truncate", res->syscall, MAX_KPROBE_NAME);

    handle_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_ftruncate")
int BPF_KRETPROBE(ftruncate) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_ftruncate);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_ftruncate*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("ftruncate", res->syscall, MAX_KPROBE_NAME);

    handle_fd(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_rename")
int BPF_KRETPROBE(rename) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_rename);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_rename*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("rename", res->syscall, MAX_KPROBE_NAME);

    handle_new_old_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_renameat")
int BPF_KRETPROBE(renameat) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_renameat);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_renameat*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("renameat", res->syscall, MAX_KPROBE_NAME);

    handle_new_old_fd_and_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_renameat2")
int BPF_KRETPROBE(renameat2) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_renameat2);
    struct syscall_args_t *saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == NULL) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_renameat2*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("renameat2", res->syscall, MAX_KPROBE_NAME);

    handle_new_old_fd_and_filename(saved_args, res);;
    log_event(ctx, res);

    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}


// Kretprobe
// open* -> save the opened files
// dup -> duplicate an opened file

SEC("kretprobe/__x64_sys_open")
int BPF_KRETPROBE(open_ret) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_open);
    struct syscall_args_t * saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);

    if (saved_args == 0) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_open*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("open", res->syscall, MAX_KPROBE_NAME);
    
    // Read the mode
    res->result.mode = (uint64_t) saved_args->args[1];

    // Read the filename in the remaining buffer
    bpf_probe_read_str(res->result.args[0], sizeof(res->result.args[0]), (void *) saved_args->args[0]);

    res->result.args[1][0] = '\0';
    res->result.args[2][0] = '\0';
    res->result.args[3][0] = '\0';
    
    // Log and save a new fd
    int retval = PT_REGS_RC(ctx);
    add_new_fd2path(retval, NULL, (char *) res->result.args[0]);

    log_event(ctx, res);
    
    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_openat")
int BPF_KRETPROBE(openat_ret) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_openat); // TODO: See __NR_openat
    struct syscall_args_t * saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);
    if (saved_args == 0) {
        return 0;
    }

    struct event_result_t *res = NULL;
    res = get_syscall_percpu_result(ctx, /*__NR_openat*/ 0);
    if (res == NULL) {
        return 0;
    }

    my_strcpy("openat", res->syscall, MAX_KPROBE_NAME);
    
    // Read the mode
    res->result.mode = (uint64_t) saved_args->args[2];

    int retval = PT_REGS_RC(ctx);

    // Read the filename in the remaining buffer
    bpf_probe_read_str(&res->result.args[1], sizeof(res->result.args[1]), (void *) saved_args->args[1]);
    add_new_fd2path(retval, NULL, (char *) res->result.args[1]);
    if (res->result.args[1][0] == '/') {
        res->result.args[0][0] = '\0';
    } else {
        // Read the fd 
        get_path_from_fd(saved_args->args[0], res->result.args[0], sizeof(res->result.args[0]));
    }

    res->result.args[2][0] = '\0';
    res->result.args[3][0] = '\0';

    log_event(ctx, res);
    
    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_dup")
int BPF_KRETPROBE(dup_ret) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_dup);
    struct syscall_args_t * saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);

    if (saved_args == 0) {
        return 0;
    }
    
    // Log and save a new fd
    int retval = PT_REGS_RC(ctx);
    clone_fd2path(retval, saved_args->args[0]);
    
    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_dup2")
int BPF_KRETPROBE(dup2_ret) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_dup2);
    struct syscall_args_t * saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);

    if (saved_args == 0) {
        return 0;
    }
    
    clone_fd2path(saved_args->args[1], saved_args->args[0]);
    
    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}

SEC("kretprobe/__x64_sys_dup3")
int BPF_KRETPROBE(dup3_ret) {
    // save parsed args from map
    u64 id = get_syscall_map_key(__NR_dup2);
    struct syscall_args_t * saved_args = bpf_map_lookup_elem(&syscall_args_map, &id);

    if (saved_args == 0) {
        return 0;
    }
    
    clone_fd2path(saved_args->args[1], saved_args->args[0]);
    
    bpf_map_delete_elem(&syscall_args_map, &id);
    return 0;
}


// ================== Licence ==================

// char LICENSE[] SEC("license") = "GPL";
char LICENSE[] SEC("license") = "Dual BSD/GPL";
