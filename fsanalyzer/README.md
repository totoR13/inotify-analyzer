# eBPF file system event analyzer 

## Prerequisites

* Update the repo dependencies
```bash
$ # update the libbpf subfolder
$ git submodule update --init 
```
* Build an Android Linux kernel (refer to the [official guide](https://source.android.com/docs/setup/build/building-kernels)) with:
```
CONFIG_KPROBES=y
CONFIG_KPROBE_EVENT=y
CONFIG_BPF_SYSCALL=y
CONFIG_IKHEADERS=m

// needed for /sys/kernel/debug/tracing/kprobe_events -- It should be mounted with: `mount -t debugfs debugfs /sys/kernel/debug`
CONFIG_DEBUG_FS=y

// optionally, but suggested (needed for libbpf)
CONFIG_DEBUG_INFO_BTF=y
```

## Build

The provided Makefile perform the following steps:
1. Build `libbpf` from the source code (header + shared object)
2. Build the eBPF script in an object file (with the Android kernel header)
3. Build the user space app (with golang)

Building the Makefile requires setting the `KERN_HEADERS` variable to the root folder of the Android (Linux) kernel:
```
$ KERN_HEADERS=</path/to/android-kernel> make
```

The Makefile accepts other args passed as environment variables:
* `BPF_GLOBAL_NAME`: the name of the eBPF script (default is `exec`);
* `VERSION`: the version of the script build (default is `0.0.1`);
* `OUT_DIR`: the output folder (default is `dist`)

The output of the build phase is 2 files:
* `${BPF_GLOBAL_NAME}_app` is the userspace app that installs the eBPF program into the kernel. When the app receives a 'CTRL-C' command, it unloads the eBPF script and saves the result in a JSON file;
* `${BPF_GLOBAL_NAME}.bpf.${VERSION}.o` is the effective eBPF program.


## Usage 

```bash
$ # Push the file into the emulator
$ adb push exec_app /data/local/tmp
$ adb push exec.bpf.0_0_1.o /data/local/tmp
$ adb root
$ adb shell
$ cd /data/local/tmp
$ ls 
exec_app
exec.bpf.0_0_1.o
$ # Run the eBPF userspace app passing the path to the eBPF program to install
$ ./exec_app --ebpfObjectFile /data/local/tmp/exec.bpf.0_0_1.o --outFile /data/local/tmp/output.json
[...]
^C
$ # Check the output
$ ls
exec_app
exec.bpf.0_0_1.o 
output.json
```


### Useful links:
* [libbpf](https://github.com/libbpf/libbpf)
* [libbpfgo](https://github.com/aquasecurity/libbpfgo)
* [libbpfgo example](https://github.com/lizrice/libbpfgo-beginners)
