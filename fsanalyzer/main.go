package main

import (
	"C"

	bpf "main/libbpfgo"

	// "bufio"
	"bytes"
	// "container/list"
	// "debug/elf"
	"encoding/binary"
	// "encoding/gob"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	// "os/exec"
	"os/signal"
	// "reflect"
	// "strconv"
	"strings"
	// "syscall"
	// "unsafe"
)

// ================== Cmd-line Arguments ==================

type cmdLineArgs struct {
    ebpfObjectFile 	string // .o file of the eBPF program to load
    outFile			string
}

func ParseCmdLine() *cmdLineArgs {
	// Register flags
	ebpfObjectFile := flag.String("ebpfObjectFile", "exec.bpf.0_0_1.o", "The path (absolute or relative) of the eBPF object file to load.")
	outFile := flag.String("outFile", "/data/local/tmp/test.json", "The absolute path of the output JSON file.")

	// Parse cmd line
	flag.Parse()

	// Validate values -> Check if file exists
	for _, file := range []string{*ebpfObjectFile/*, *outFile*/} {
		if _, err := os.Stat(file); errors.Is(err, os.ErrNotExist) {
			panic("File " + file + " does not exists.")
		}
	}
	
	return &cmdLineArgs{ebpfObjectFile: *ebpfObjectFile, outFile: *outFile}
}

// ================== JSON ==================

// TODO: save result events to a json file
func WriteJsonResult(eventResults []EventResultParsed, outFile string) {
	file, _ := json.MarshalIndent(eventResults, "", " ")
	_ = ioutil.WriteFile(outFile, file, 0644)
}


// ================== eBPF Utility ==================

const EBPF_KPROBE_NAME_SIZE = 10
const EBPF_STRING_SIZE = 128*4
const EBPF_RESULT_ARG_SIZE = 4

type OpenResult struct {
	Mode 			uint64
	Args 			[EBPF_RESULT_ARG_SIZE][EBPF_STRING_SIZE]byte
}

type EventResult struct {
	Timestamp 		uint64
	Pid				uint64
	Uid				uint64
	// Ppid 			uint64
	// Counter			uint32
	Syscall			[EBPF_KPROBE_NAME_SIZE]byte
	Result			OpenResult
}

type EventResultParsed struct {
	Timestamp 		uint64
	Pid				uint64
	Uid				uint64
	// Ppid			uint64
	// Counter			uint32
	Syscall			string
	Mode			uint64
	Args			[EBPF_RESULT_ARG_SIZE]string
}

func ConvertStructToByteArray(data interface{}) ([]uint8, error) {
	var buffer bytes.Buffer
	err := binary.Write(&buffer, binary.BigEndian, data) // LittleEndian
	
	return buffer.Bytes(), err
}

func ConvertStringToEbpfArray(str string) [EBPF_STRING_SIZE]byte {
	strBytes := []byte(str)
	var bs [EBPF_STRING_SIZE]byte
	copy(bs[:], strBytes)
	return bs
}

func InitBpfMaps(bpfModule *bpf.Module) {
	m, _ := bpfModule.GetMap("result_map")
	/*syscalls := []int {
		, // syscall.SYS_CHMOD, 
		, // syscall.SYS_CHOWN, 
		3, // syscall.SYS_CLOSE, 
		, // syscall.SYS_EXECVE, 
		, // syscall.SYS_EXECVEAT,
		, // syscall.SYS_FCHMOD, 
		, // syscall.SYS_FCHMODAT, 
		, // syscall.SYS_FCHOWN, 
		, // syscall.SYS_FCHOWNAT, 
		, // syscall.SYS_FSETXATTR, 
		0x4d, // syscall.SYS_FTRUNCATE, 
		, // syscall.SYS_FUTIMESAT, 
		, // syscall.SYS_LCHOWN, 
		0x56, // syscall.SYS_LINK, 
		0x109, // syscall.SYS_LINKAT, 
		0xbd, // syscall.SYS_LSETXATTR, 
		, // syscall.SYS_MKDIR, 
		, // syscall.SYS_MKDIRAT, 
		, // syscall.SYS_MKNOD, 
		, // syscall.SYS_MKNODAT, 
		, // syscall.SYS_READ, 
		, // syscall.SYS_RENAME, 
		, // syscall.SYS_RENAMEAT, 
		, // syscall.SYS_RENAMEAT2, 
		, // syscall.SYS_RMDIR, 
		0xbc, // syscall.SYS_SETXATTR, 
		0x58, // syscall.SYS_SYMLINK, 
		0x4c, // syscall.SYS_TRUNCATE, 
		0x57, // syscall.SYS_UNLINK, 
		0x107, // syscall.SYS_UNLINKAT, 
		0x84, // syscall.SYS_UTIME, 
		0x118, // syscall.SYS_UTIMENSAT, 
		0xeb, // syscall.SYS_UTIMES, 
		1, // syscall.SYS_WRITE,
		2, // open
		0x101, // openat
	}

	for _, sysId := range syscalls {*/
		sysId := 0 // I do not need a specific for each syscall, but only for cpu! --> ebpf is not preemptable: https://lwn.net/Articles/812503/
		var eventResult EventResult
		buffer, err := ConvertStructToByteArray(eventResult)
		if err != nil {
			fmt.Printf("Error on \"ConvertStructToByteArray\". Message: %v\n", err)
			panic(err)
		}
		m.Update(uint64(sysId), buffer)
	// }

}

func RegisterSyscalls(bpfModule *bpf.Module /*, syscalls []string*/) {
	// Register sys_enter and sys_exit!
	prog_enter, err := bpfModule.GetProgram("fun__sys_enter")
	if err != nil {
		panic(err)
	}
	_, err = prog_enter.AttachRawTracepoint("sys_enter")
	if err != nil {
		panic(err)
	}

	// prog_exir, err := bpfModule.GetProgram("fun__sys_exit")
	// if err != nil {
	// 	panic(err)
	// }
	// _, err = prog_exir.AttachRawTracepoint("sys_exit")
	// if err != nil {
	// 	panic(err)
	// }

	// Register kprobe
	// syscalls := map[string]int{
	// 	// "access": "access",
	// 	// "faccessat": "faccessat",
	// 	"close", "close"
	// 	"read": ""
	// } 

	syscalls := []string{/*"access", "faccessat",*/ 
		"chmod", 
		"chown", 
		"close", 
		"execve", 
		"execveat",
		"fchmod", 
		"fchmodat", 
		"fchown", 
		"fchownat", 
		"fsetxattr", 
		"ftruncate", 
		"futimesat", 
		"lchown", 
		"link", 
		"linkat", 
		"lsetxattr", 
		"mkdir", 
		"mkdirat", 
		"mknod", 
		"mknodat", 
		"read", 
		"rename", 
		"renameat", 
		"renameat2", 
		"rmdir", 
		"setxattr", 
		"symlink", 
		"truncate", 
		"unlink", 
		"unlinkat", 
		"utime", 
		"utimensat", 
		"utimes", 
		"write",
	} 
	for _, syscall := range syscalls {
	// for syscall, handler := range syscalls {
		prog_kprobe, err := bpfModule.GetProgram(syscall)
		if err != nil {
			panic(err)	
		}
		_, err = prog_kprobe.AttachKretprobe("__x64_sys_" + syscall) // AttachKprobe
		if err != nil {
			panic(err)
		}
	}

	// Register kretprobes
	syscalls = []string{"open", "openat", "dup", "dup2", "dup3"}
	for _, syscall := range syscalls {
		prog_kretprobe, err := bpfModule.GetProgram(syscall + "_ret")
		if err != nil {
			panic(err)	
		}
		_, err = prog_kretprobe.AttachKretprobe("__x64_sys_" + syscall)
		if err != nil {
			panic(err)
		}
	}

}

// TODO:
// -) Error:
// 		libbpf: map 'event_result_perf_map' (legacy): legacy map definitions are deprecated, use BTF-defined maps instead
// 		libbpf: Use of BPF_ANNOTATE_KV_PAIR is deprecated, use BTF-defined maps in .maps section instead

// ================== Main ==================

// See also: https://github.com/cilium/ebpf
func main() {
	// Parse cmd line args
	args := ParseCmdLine()

	fmt.Println("ebpfObjectFile: ", args.ebpfObjectFile)

	// Parse the JSON file and retrieve the stats
	// config, err := ParseConfigJson(args.configFile)
	// if err != nil {
	// 	panic(err)
	// }

	// Signal for avoid to teminate the user-space program
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)

	// Load eBPF object file 
	bpfModule, err := bpf.NewModuleFromFile(args.ebpfObjectFile)
	if err != nil {
		panic(err)
	}

	// Note: A defer statement defers the execution of a function until the surrounding function returns.
	defer bpfModule.Close()

	err = bpfModule.BPFLoadObject()
	if err != nil {
		panic(err)
	}

	// Init maps
	InitBpfMaps(bpfModule)

	// Register function to retrieve events from ebpf
	buf_api := make(chan []byte)
	pb_api, err := bpfModule.InitPerfBuf("event_result_perf_map", buf_api, nil, 4096)
	if err != nil {
		panic(err)
	}

	pb_api.Start()

	// eventResults := list.New()
	eventResults := []EventResultParsed{}
	go func() {
		for raw := range buf_api {
			// fmt.Println("========== Received raw data ==========") //, raw

			var res EventResult
			var dataBuffer *bytes.Buffer

			dataBuffer = bytes.NewBuffer(raw)
			err = binary.Read(dataBuffer, binary.LittleEndian, &res)

			// fmt.Println("Complete obj", raw)
			// fmt.Println("Timestamp", res.Timestamp)
			// fmt.Println("Pid", res.Pid)
			// // fmt.Println("Ppid", res.Ppid)
			// fmt.Println("Uid", res.Uid)
			// fmt.Println("Name", string(res.Syscall[:]))
			// fmt.Println("res.Result.Mode", res.Result.Mode)
			// for i := 0;  i < EBPF_RESULT_ARG_SIZE; i++ {
			// 	fmt.Println("res.Result.Args[", string(i), "]", string(res.Result.Args[i][:]))
			// }

			syscall := string(res.Syscall[:])
			syscall = syscall[:strings.Index(syscall, "\x00")]
			
			parsed := EventResultParsed{ // pb == &Student{"Bob", 8}
				Timestamp: res.Timestamp,
				Pid: res.Pid,
				Uid: res.Uid,
				// Ppid: res.Ppid,
				// Counter: res.Counter,
				Syscall: syscall,
				Mode: res.Result.Mode,
			}

			for i := 0;  i < EBPF_RESULT_ARG_SIZE; i++ {
				str := string(res.Result.Args[i][6:]) // 2
				str = str[:strings.Index(str, "\x00")]
				parsed.Args[i] = str
			}

			eventResults = append(eventResults, parsed)
		}
	}()

	defer pb_api.Stop()

	// Register sys_enter and sys_exit
	RegisterSyscalls(bpfModule)

	// Avoid termination
	<-sig

	WriteJsonResult(eventResults, args.outFile)
}